FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
WORKDIR /admin
COPY --from=build-stage /app/dist /admin
COPY ./nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

#FROM nginx:stable-alpine as production-stage
#COPY --from=build-stage /app/dist /admin
#EXPOSE 3000
#CMD ["nginx", "-g", "daemon off;"]
